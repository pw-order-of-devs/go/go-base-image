FROM golang:1.18-alpine

RUN apk update && apk --no-cache add git curl musl-dev gcc
RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin
